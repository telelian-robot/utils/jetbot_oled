import subprocess


def get_cpu_usage():
    cmd = "top -bn1 | grep load | awk '{printf \"CPU Load: %.2f\", $(NF-2)}'"
    return subprocess.check_output(cmd, shell = True ).decode('ascii')[:-1]

def get_disk_usage():
    cmd = "df -h | awk '$NF==\"/\"{printf \"Disk:%d/%dGB %s\", $3,$2,$5}'"
    return subprocess.check_output(cmd, shell = True ).decode('ascii')[:-1]

def get_mem_usage():
    cmd = "free -m | awk 'NR==2{printf \"Mem:%s/%sM %.2f%%\", $3,$2,$3*100/$2 }'"
    return subprocess.check_output(cmd, shell = True ).decode('ascii')[:-1]
    