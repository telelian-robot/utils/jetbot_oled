import logging
from time import sleep
import Adafruit_SSD1306

from pkg_resources import resource_filename
from PIL import Image, ImageDraw, ImageFont

class JetbotOled():
    def __init__(self, i2c_bus=7):
        logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s' ,level="INFO")
        logging.info('init jetbot oled')
        self.__oled = Adafruit_SSD1306.SSD1306_128_32(rst=None, i2c_bus=i2c_bus, gpio=1)
        self.__oled.begin()
        self.__oled.clear()
        self.__oled.display()
                
        self.__font = ImageFont.load_default()
        
        self.__width = self.__oled.width
        self.__height = self.__oled.height
        
        self.__padding = -2
        # self.__padding = 0
        self.__top = self.__padding
        self.__bottom = self.__height-self.__padding
        self.__x = 0
        
        self.__image = Image.new('1', (self.__width, self.__height))
        
    def render(self, text: list):
        draw = ImageDraw.Draw(self.__image)
        draw.rectangle((0,0,self.__width, self.__height), outline=0, fill=0)
        top = self.__top
        x = self.__x
        font = self.__font
        for line in text:
            draw.text((x, top), line, font=font, fill=255)
            top += 8
        
        self.__oled.image(self.__image)
        self.__oled.display()
        
    def clear(self):
        self.__oled.clear()
        self.__oled.display()
    
    def __del__(self):
        logging.info('close jetbot oled')
        self.clear()
        
if __name__ == '__main__':
    from network import get_ip_address
    from stats import get_cpu_usage, get_disk_usage, get_mem_usage
    from power import StatPower
    
    stat_power = StatPower(i2c_dev_num='7-0040', hwmon_dev_num='hwmon5')
    
    oled = JetbotOled(i2c_bus=1)
    while True:
        ip = get_ip_address('wlan0')
        if ip:
            ip_text = f'wlan0:{ip}'
        else:
            ip_text = f'eth0:{get_ip_address("eth0")}'
        v_text, pi_text = stat_power.get_stats()
        text = [
            ip_text,
            str(get_mem_usage()),
            v_text,
            pi_text,
        ]
        oled.render(text)
        sleep(1)
        
    