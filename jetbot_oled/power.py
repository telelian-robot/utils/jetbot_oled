import subprocess
import os

class StatPower():
    def __init__(self, driver_name='ina2xx', i2c_dev_num='7-0040', hwmon_dev_num='hwmon5'):
        self.__driver = driver_name
        self.__i2c_dev = i2c_dev_num
        self.__hwmon = hwmon_dev_num
        self.__path = f'/sys/class/hwmon/{hwmon_dev_num}'
    
    def __get_device_state(self):
        paths = [
            f'/sys/bus/i2c/drivers/{self.__driver}',
            f'/sys/bus/i2c/drivers/{self.__driver}/{self.__i2c_dev}',
            f'/sys/class/hwmon/{self.__hwmon}'
        ]
        
        for path in paths:
            if not os.path.exists(path):
                return None
        return True
    
    def get_stats(self):
        if not self.__get_device_state():
            return None
        cmd = f'cat {self.__path}/in0_input {self.__path}/in1_input {self.__path}/curr1_input  {self.__path}/power1_input'
        output = subprocess.check_output(cmd, shell=True).decode('ascii')[:-1]
        output = output.split('\n')  
        return (f'Bt:{int(output[1])/1000:.2f}V V:{int(output[0])}mV'
                ,f'I:{int(output[2])/1000:.2f}A P:{int(output[3])/1000000:.2f}W')
    
    def get_stats_int(self):
        if not self.__get_device_state():
            return None
        cmd = f'cat {self.__path}/in0_input {self.__path}/in1_input {self.__path}/curr1_input  {self.__path}/power1_input'
        output = subprocess.check_output(cmd, shell=True).decode('ascii')[:-1]
        output = output.split('\n')  
        return (f'Bt:{int(output[1])} V:{int(output[0])} I:{int(output[2])} P:{int(output[3])}')
    

if __name__ == '__main__':
    stat_power = StatPower()
    # print(get_ina2xx_interface_state())
    print(stat_power.get_stats())
    
